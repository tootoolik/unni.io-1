@extends('layouts.app')
@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="mainBlocks domains">
        <div class="col-4 tablet indent-up-down-10">
            <p>
                <strong class="organizers-categories-main-title">Категории</strong>
            </p>

            <hr>

            <div class="organizers-categories">
                <div class="row organizers-categories-block">
                    <div class="col-lg-6 col-md-8 col-sm-9 col-xs-9">
                        <span class="organizers-categories-title {{ (session('organizer_filter_tag') === null || session('organizer_filter_tag') === 'all') ? 'organizers-categories-title-active' : '' }}" data-type="tag" data-sort="all">
                            Все
                        </span>
                    </div>
                </div>
                @foreach($categories as $category)
                    <div class="row organizers-categories-block">
                        <div class="col-lg-6 col-md-8 col-sm-9 col-xs-9">
                            <span class="organizers-categories-title {{ session('organizer_filter_tag') == $category->tags_id ? 'organizers-categories-title-active' : '' }}" data-type="tag" data-sort="{{ $category->tags_id }}">
                                {{ $category->tags->tag ?? '' }}
                            </span>
                        </div>
                        <div class="col-lg-5 col-md-4 col-sm-3 col-xs-3 text-right">
                            <span class="organizers-categories-count">{{ $category->count_tags }}</span>
                        </div>
                    </div>
                @endforeach
            </div>

            </div>

        <div class="col-8 searchPageAjax tablet">
            @include('Organizer.load')
        </div>
        <div class="loader" id="loader" data-page="1"></div>
    </div>

@endsection
