@extends('layouts.app')

@section('content')
<div class="container pageTags">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <h3 class="panel-heading textCenter">Добавить сайт в индекс</h3>
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <div class="row">
                        <div class="customForm col-lg-12 add-site">

                            <div class="input mysearchbar">

                                <form method="POST" action="{{ route('front.profile.site.store') }}">
                                    {{ csrf_field() }}

                                    <div class="easy-autocomplete">
                                        <input type="text" name="domain">
                                    </div>

                                    <button class="header__searchButton">Добавить</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
