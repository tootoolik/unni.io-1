{{$adminURL = env('APP_ADMIN_URL', 'test')}}
        <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap core CSS-->
    <link href="/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="/admin/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/admin/vendor/font-awesome/css/font-awesome-animation.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/admin/css/sb-admin.css" rel="stylesheet">
    <link href="/admin/css/app.css?v=1.0" rel="stylesheet">
    <link href="{{ asset('css/grid.css') }}" rel="stylesheet">

    {{--<link href="/admin/vendor/tagging/tag-basic-style.css" rel="stylesheet">--}}

    <script src="/admin/vendor/jquery/jquery.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>

    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js" defer></script>


    <link href="{{ asset('css/admin.css?v=1.3') }}" rel="stylesheet">

    <script>
        window.adminURL = "{{ $adminURL }}";
    </script>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<div id="app">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="/{{$adminURL}}">GIID SEARCH SYSTEM</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link" href="/{{$adminURL}}">
                        <i class="fa fa-fw fa-dashboard"></i>
                        <span class="nav-link-text">Главная</span>
                    </a>
                </li>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i> Домены
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        <a class="dropdown-item" href="{{ route('admin.domains.type', 'admin') }}">Первая очередь</a>
                        <a class="dropdown-item" href="{{ route('admin.domains.type', 'parser') }}">Вторая очередь</a>
                        <a class="dropdown-item" href="{{ route('admin.domains.type', 'user') }}">От пользователя</a>
                        {{--<a class="dropdown-item" href="{{ route('admin.domains', 'parser') }}">Вторая очередь</a>--}}
                        {{--<a class="dropdown-item" href="{{ route('admin.domains', 'user') }}">От пользователя</a>--}}
                    </div>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                    <a class="nav-link" href="/{{$adminURL}}/tags">
                        <i class="fa fa-fw fa-table"></i>
                        <span class="nav-link-text">Теги</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
                    <a class="nav-link" href="/{{$adminURL}}/proxy">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">Прокси сервера</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
                    <a class="nav-link" href="/{{$adminURL}}/hint_with_dictionary" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-file"></i>
                        <span class="nav-link-text">Словарь для подсказок</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
                    <a class="nav-link" href="/{{$adminURL}}/search_history" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-sitemap"></i>
                        <span class="nav-link-text">История поиска</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                    <a class="nav-link" href="/{{$adminURL}}/pages">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Страницы
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                    <a class="nav-link" href="/{{$adminURL}}/demons">
                        <i class="fa fa-fw fa-link"></i>Демоны
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                    <a class="nav-link" href="/{{$adminURL}}/organizers">
                        <i class="fa fa-fw fa-link"></i>Организаторы
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                    <a class="nav-link" href="/{{$adminURL}}/channels">
                        <i class="fa fa-fw fa-link"></i>Каналы
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
                    <a class="nav-link" href="/{{$adminURL}}/posts">
                        <i class="fa fa-fw fa-link"></i>Публикации
                    </a>
                </li>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-cogs" aria-hidden="true"></i> Настройки
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        <a class="dropdown-item" href="/{{$adminURL}}/settings/paser">Парсер</a>
                        <a class="dropdown-item" href="/{{$adminURL}}/settings/paser/create">Создать парсер</a>
                        <a class="dropdown-item" href="{{ route('admin.settings.tags') }}">Теги</a>
                    </div>
                </li>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i> Статистика
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        <a class="dropdown-item" href="{{ route('admin.statistics.tags') }}">Теги</a>
                        <a class="dropdown-item" href="{{ route('admin.statistics.domains')}}">Домены</a>
                        <a class="dropdown-item" href="{{ route('admin.statistics.click.links')}}">Ссылки</a>
                        <a class="dropdown-item" href="{{ route('admin.statistics.parser.logs')}}">Логи парсера</a>
                    </div>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
                    <a class="nav-link" href="{{ route('admin.seo.index') }}">
                        <i class="fas fa-bullhorn"></i>
                        <span class="nav-link-text">SEO модуль</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-envelope"></i>
                        <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
                        <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                        <h6 class="dropdown-header">New Messages:</h6>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <strong>David Miller</strong>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <strong>Jane Smith</strong>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <strong>John Doe</strong>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item small" href="#">View all messages</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-bell"></i>
                        <span class="d-lg-none">Alerts
              <span class="badge badge-pill badge-warning">6 New</span>
            </span>
                        <span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">New Alerts:</h6>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
              <span class="text-success">
                <strong>
                  <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
              <span class="text-danger">
                <strong>
                  <i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
              </span>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
              <span class="text-success">
                <strong>
                  <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
                            <span class="small float-right text-muted">11:21 AM</span>
                            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item small" href="#">View all alerts</a>
                    </div>
                </li>
                <li class="nav-item">
                    <form class="form-inline my-2 my-lg-0 mr-lg-2">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Search for...">
                            <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
                        </div>
                    </form>
                </li>
                <li class="nav-item">
                    <a lass="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/test">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">My Dashboard</li>
            </ol>
            @foreach ($errors->all() as $error)
               <div class="errors_block">
                   <li class="lert alert-danger col-lg-12">{{ $error }}</li>
               </div>
            @endforeach
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @yield('content')
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-latest.min.js"></script>
    <script src="/admin/vendor/bootstrap/js/bootstrap.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/admin/js/app.js?v=1.1"></script>

    <link href="/admin/vendor/tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
    <script src="/admin/vendor/tagsinput/bootstrap-tagsinput.js"></script>
    @yield('footer_script')
    <script>
        $('#tagPlaces').tagsinput({
            allowDuplicates: true
        });
    </script>
    {{--<script src="/admin/vendor/tagging/tagging.js"></script>--}}
    <!-- Bootstrap core JavaScript-->

    {{--<script src="/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
    {{--<!-- Core plugin JavaScript-->--}}
    {{--<script src="/admin/vendor/jquery-easing/jquery.easing.min.js"></script>--}}
    {{--<!-- Page level plugin JavaScript-->--}}
    {{--<script src="/admin/vendor/chart.js/Chart.min.js"></script>--}}
    {{--<!-- Custom scripts for all pages-->--}}
    {{--<script src="/admin/js/sb-admin.min.js"></script>--}}
    {{--<!-- Custom scripts for this page-->--}}
    {{--<script src="/admin/js/sb-admin-charts.min.js"></script>--}}

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}"></script>--}}
</div>
<script src="{{ asset('js/admin.js?v=1.1') }}"></script>
</body>

</html>
