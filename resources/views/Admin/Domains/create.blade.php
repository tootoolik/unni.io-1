{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 domainPage">
                <h1 class="col-lg-12">Добавление домена</h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post" action="{{ route('admin.domains.store') }}">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="">Имя домена</label>
                                        <input class="form-control" type="text" name="domain" value="{{ old('domain') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Приоритет</label>
                                        <input class="form-control" type="text" name="priority" value="{{ old('priority') }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" class="checkbox" name="enableDomain" />
                                    </div>
                                </div>

                                <div class="col-lg-10 tags-input-tokenize2-create">
                                    <label for="">Теги домена</label>
                                    <select class="tags-input" multiple name="tags[]">
                                    </select>
                                    <small class="create-new-tag">
                                        <a href="{{ route('admin.tags.add_tags') }}" target="_blank">Не нашли нужный тег? Создайте</a>
                                    </small>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="checkbox" class="checkbox" name="enableDomain" id="parsing-site-checkbox" value="1"/>
                                    <label for="parsing-site-checkbox">Парсить сайт</label>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="checkbox" class="checkbox" name="status_delete" id="delete-content" value="1"/>
                                    <label for="delete-content">Удалять контент</label>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="submit" value="Сохранить" class="btn btn-success">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection