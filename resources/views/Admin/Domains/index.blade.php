{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    <style>
        .domains-general-data-text {
            border: 2px solid #e9ecef;
            padding: 9px;
            margin-left: 10px;
            border-radius: 4px;
            vertical-align: middle;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <h1>Список доменов для парсера</h1>
            <div class="form-group domains-general-data">
                <a class="btn btn-success" href="{{ route('admin.domains.create') }}">Добавить домен для парсинга</a>
                <span class="domains-general-data-text">Общее количество контента: <strong>{{$countIndexis}}</strong></span>
                <span class="domains-general-data-text">Общее количество ссылок: <strong>{{$countIndexisUrls}}</strong></span>
                <span class="domains-general-data-text">Текущая итерация: <strong>xxx</strong></span>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Домен</th>
                            <th>Канал</th>
                            <th>Приоритет домена</th>
                            <th>Найдено раз</th>
                            <th>Кол-во итераций</th>
                            <th>Кол-во ссылок</th>
                            <th>Кол-во контента</th>
                            <th>Парсер контента</th>
                            <th>Ошибки</th>
                            <th>Ping</th>
                            <th>Создан</th>
                            <th width="100">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    {{ $category->id }}
                                </td>
                                <td>{{$category->domain}} <a href="{{ route('front.domains', $category->domain) }}"
                                                             target="_blank">
                                        <i class="fa fa-external-link" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td>
                                    @if(count($category->manualChannel)>0 )
                                        @foreach($category->manualChannel as $channel)
                                            <a href="/test/channels/edit/{{$channel->id }}" target="_blank">
                                                <button class="btn btn-sm btn-info">{{$channel->name }}</button>
                                            </a>
                                        @endforeach
                                    @else
                                        <a href="/test/channels/create?domain_id={{$category->id }}&domain_name={{$category->domain}}" target="_blank">
                                            <button class="btn btn-sm btn-success">+ канал</button>
                                        </a>
                                    @endif
                                </td>
                                <td>{{$category->priority}}</td>
                                <td>{{$category->found_count}}</td>
                                <td>{{$category->count}}</td>
                                <td>
                                    {{$category->countContent['count_link']}}
                                </td>
                                <td>
                                    {{$category->countContent['count_content']}}
                                </td>
                                <td>
                                    <form action="" method="POST" class="enableDomain">
                                        @if($category->parser_content == '1')
                                            <input type="checkbox" class="checkbox" name="enableDomain"
                                                   id="{{$category->id}}" checked/>
                                        @else
                                            <input type="checkbox" class="checkbox" name="enableDomain"
                                                   id="{{$category->id}}"/>
                                        @endif
                                        <label for="{{$category->id}}"></label>
                                    </form>
                                </td>
                                <td>
                                    <a href="/{{ $adminURL }}/domains/{{$category->id}}" class="btn btn-xs btn-default">
                                        Подробнее
                                    </a>
                                </td>
                                <td>{{ $category->ping }}</td>
                                <td>{{ $category->created_at}}</td>
                                <td>
                                    <a href="/{{ $adminURL }}/domains/{{$category->id}}/edit">
                                        <button class="btn btn-sm btn-success" type="submit">Редактировать</button>
                                    </a>
                                    <form action="{{route('admin.domains.destroy', $category->id)}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-sm btn-danger" type="submit">Удалить</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links('paginate') }}
                </div>
            </div>
        </div>
    </div>

@endsection
