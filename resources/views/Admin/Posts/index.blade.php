@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="form-group">Список публикаций</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="{{ route('admin.posts.moderation') }}" class="btn btn-danger">Публикации на модерации</a>
                    <a href="{{ route('admin.posts.approved') }}" class="btn btn-success">Одобренные публикации</a>
                    <a href="{{ route('admin.posts.create') }}" class="btn btn-success">Добавить новую публикацию</a>
                    <br>
                    <br>
                    @if( session()->has('success') && !empty(session('success')))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Описание</th>
                            <th>Изображение</th>
                            <th>Дата создания</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->id}}</td>
                                <td>{{$post->name}}</td>
                                <td>{{$post->body}}</td>
                                <td>
                                    @if( $post->post_img != NULL || $post->post_img != '' )
                                        <img src="/storage/{{$post->post_img}}" alt="img" width="150px" class="pb-3">
                                    @endif
                                </td>
                                <td>{{$post->created_at}}</td>
                                <td>{{$post->active}}</td>
                                <td>
                                    <a href="{{ route('admin.posts') }}/edit/{{$post->id}}" class="btn btn-xs btn-outline-secondary">
                                        Редактировать
                                    </a>
                                    <a href="{{ route('admin.posts') }}/destroy/{{$post->id}}" class="btn btn-xs btn-danger">
                                        Удалить
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $posts->links('paginate') }}
                </div>
            </div>
        </div>
    </div>

@endsection