{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    {{--@if (session('status'))--}}
        {{--<div class="alert alert-success">--}}
            {{--{{ session('status') }}--}}
        {{--</div>--}}
    {{--@endif--}}

    <div class="row">
        <div class="col-12">
            <h1>Добавление тегов</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/{{$adminURL}}/tags/add_tags">
                        {{ csrf_field() }}
                        <div class="form-group addTag">
                            <label for="addTag" class="addTagBlocks">
                                Укажите тег - поисковый запрос
                                <div class="form-group">
                                    <input type="text" name="addTag[]" value="" class="form-control">
                                </div>
                            </label>
                        </div>
                        <input type="button" class="change btn btn-success tagAddButton" value="+ Добавить поле">
                        <input type="hidden" name="tagType" value="addHand">
                        <button class="change btn btn-success tagSend">
                            Добавить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection