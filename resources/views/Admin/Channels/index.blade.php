@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="form-group">Список каналов</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="channels/create" class="btn btn-success">Добавить канал</a>
                    <br>
                    <br>

                    @if( session()->has('success') && !empty(session('success')))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Описание</th>
                            <th>FB id</th>
                            <th>Адрес сайта</th>
                            <th>Дата создания</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($channels as $channel)
                            <tr>
                                <td>{{$channel->id}}</td>
                                <td>{{$channel->name}}</td>
                                <td>{{$channel->body}}</td>
                                <td>{{$channel->fb_group_id}}</td>
                                <td><a href="{{$channel->website}}">Перейти</a></td>
                                <td>{{$channel->created_at}}</td>
                                <td>{{$channel->active}}</td>
                                <td>
                                    <a href="channels/destroy/{{$channel->id}}" class="btn btn-xs btn-danger">
                                        Удалить
                                    </a>
                                    <a href="channels/edit/{{$channel->id}}" class="btn btn-xs btn-outline-secondary">
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
{{--                    <a href="channels/generate" class="btn btn-success">Сгенерировать каналы</a>--}}
                    <br>
                    {{ $channels->links('paginate') }}
                </div>
            </div>
        </div>
    </div>

@endsection
