@extends('Admin/layouts.app')
@section('content')

    <div class="row">
        <div class="col-12">
            <h1>Редактировать запись</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="POST" action="{{ $channel->id }}">

                        <div class="form-group col-lg-4">
                            <label for="">Название</label>
                            <input type="text" name="name" class="form-control" value="{{ isset($channel) ? $channel->name : '' }}">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Домен</label>
                            <select class="domain-input" multiple name="domains[]">
                                @if(isset($domains))
                                    @foreach ($domains as $domain)
                                        <option value="{{ $domain->id }}" selected>{{ $domain->domain }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Ссылка на сайт</label>
                            <input type="text" name="website" class="form-control" value="{{ isset($channel) ? $channel->website : '' }}">
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">FB id</label>
                            <input type="text" name="fb_group_id" class="form-control" value="{{ isset($channel) ? $channel->fb_group_id : '' }}">
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="description">Описание</label>
                            <textarea class="form-control" name="chanel_content" id="chanel_content">{{ isset($channel) ? $channel->body : ''}}</textarea>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Теги</label>
                            <select class="tags-input" multiple name="tags[]">
                                @if(isset($tags))
                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" selected>{{ $tag->tag }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Оотображать в списке каналов?</label>
                            <select class="form-control" name="public">
                                @if($channel->active == 1)
                                    <option selected value="1">Да</option>
                                    <option value="0">Нет</option>
                                @elseif($channel->active == 0)
                                    <option value="1">Да</option>
                                    <option selected value="0">Нет</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <button class="btn btn-success">Сохранить</button>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'chanel_content',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
    <link href="/admin/css/tokenize2.min.css" rel="stylesheet">
    <script src="/admin/js/tokenize2.min.js"></script>
@stop