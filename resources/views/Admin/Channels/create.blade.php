@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Создать новый канал</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="store" method="post">

                        <div class="form-group col-lg-4">
                            <label for="">Название</label>
                            <input type="text" name="name" class="form-control"
                                   value="{{$data->domain_id?$data->domain_name:''}}"
                            >
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Домен</label>
                            <select class="domain-input" multiple name="domains[]">
                                @if($data->domain_id)
                                    <option value="{{$data->domain_id}}" selected="selected">{{$data->domain_name}}</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Ссылка на сайт</label>
                            <input type="text" name="website" class="form-control"
                                   value="{{$data->domain_id?$data->domain_name:''}}"
                            >
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">FB id</label>
                            <input type="text" name="fb_group_id" class="form-control">
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="content">Описание</label>
                            <textarea class="form-control" name="chanel_content" id="chanel_content"></textarea>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Теги</label>
                            <select class="tags-input" multiple name="tags[]">

                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <label for="">Оотображать в списке каналов?</label>
                            <select class="form-control" name="public">
                                <option value="1">Да</option>
                                <option value="0">Нет</option>
                            </select>
                        </div>

                        <div class="form-group col-lg-4">
                            <button class="btn btn-success">Добавить новый канал</button>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script src="{{ asset('/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8"></script>
    <script>
        var editor = CKEDITOR.replace('chanel_content', {
            filebrowserBrowseUrl: '/elfinder/ckeditor'
        });
    </script>
    <script src="/admin/vendor/select/selectstyle.js"></script>
    <link href="/admin/vendor/select/selectstyle.css" rel="stylesheet" type="text/css">
@stop
