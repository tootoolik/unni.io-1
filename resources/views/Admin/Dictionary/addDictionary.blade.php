{{$adminURL = env('APP_ADMIN_URL', 'admin')}}
@extends('Admin.layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Увеличение словаря</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/{{$adminURL}}/hint_with_dictionary/add" method="post">
                        {{ csrf_field() }}
                        <div class="form-group addDictionary">
                            Внесите список
                            <div class="form-group">
                                <textarea name="addDictionary" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <button class="change btn btn-success tagSend">
                            Добавить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection