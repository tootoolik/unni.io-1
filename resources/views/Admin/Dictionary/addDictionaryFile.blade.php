@extends('Admin.layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Увеличение словаря</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/{{ env('APP_ADMIN_URL') }}/hint_with_dictionary/add_file" method="post" enctype="multipart/form-data" id="photo-form">
                        {{ csrf_field() }}
                        <div class="form-group addDictionary">
                            <h4>Загрузите файл</h4>
                            <div class="form-group">
                                <input type='file' id='files' name='files' multiple='multiple' />
                            </div>
                            <p><input type='submit' value='Загрузить' class="change btn btn-success tagSend"/></p>
                        </div>
                    </form>
                    <div class="col-lg-4">
                        <div class="alert-success"><div class="col-lg-4"><p id='progress'></p></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('form').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {

                    var xhr = new XMLHttpRequest();
                    var total = 0;
                    $.each(document.getElementById('files').files, function(i, file) {
                        total += file.size;
                    });
                    xhr.upload.addEventListener("progress", function(evt) {
                        var loaded = (evt.loaded / total).toFixed(2)*100;
                        $('#progress').text('Uploading... ' + loaded + '%' );
                    }, false);

                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                success: function(data) {
                    $('#progress').text('Файл загружен - '+data+' слов');
                }
            });
        });
    </script>
@endsection