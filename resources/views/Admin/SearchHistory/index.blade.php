{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>История поиска</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Дата добавления</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($searchHistory as $history)
                            <tr>
                                <td>{{$history->id}}</td>
                                <td>{{$history->title}}</td>
                                <td>{{$history->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $searchHistory->links('paginate') }}
                </div>
            </div>
        </div>
    </div>
@endsection