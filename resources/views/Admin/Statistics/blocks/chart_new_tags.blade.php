<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawStuff);

    function drawStuff() {
        var chartDiv = document.getElementById('chart_new');

        var data = google.visualization.arrayToDataTable([
            ['Galaxy', 'Новые теги'],
                @foreach($tagsNew as $tag)
            ['{{ $tag->date }}', {{ $tag->views }}],
            @endforeach
        ]);

        var materialOptions = {
            width: 600,
            series: {
                0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
            },
            axes: {
                y: {
                    distance: {label: 'parsecs'}, // Left y-axis.
                    brightness: {side: 'right', label: 'apparent magnitude'} // Right y-axis.
                }
            }
        };

        var classicOptions = {
            width: 900,
            series: {
                0: {targetAxisIndex: 0},
                1: {targetAxisIndex: 1}
            },
            title: 'Nearby galaxies - distance on the left, brightness on the right',
            vAxes: {
                // Adds titles to each axis.
                0: {title: 'parsecs'},
                1: {title: 'apparent magnitude'}
            }
        };

        function drawMaterialChart() {
            var materialChart = new google.charts.Bar(chartDiv);
            materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
            button.onclick = drawClassicChart;
        }

        function drawClassicChart() {
            var classicChart = new google.visualization.ColumnChart(chartDiv);
            classicChart.draw(data, classicOptions);
            button.onclick = drawMaterialChart;
        }

        drawMaterialChart();
    };
</script>
<div id="chart_new" style="width: 800px; height: 500px;"></div>