@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container tagsPage">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="panel-body delete-parser">
                        <h1>Переходы по внешним ссылкам</h1>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-xs-12">
                                        @foreach($links as $link)
                                            <div class="linkClick">
                                                <div>
                                                    <span>IP:</span> {{ $link->ip }}
                                                </div>
                                                <div>
                                                    <span>Источник перехода:</span> {{ $link->source_page }}
                                                </div>
                                                <div>
                                                    <span>Ссылка для перехода:</span> {{ $link->link }}
                                                </div>
                                                <div>
                                                    <span>Дата:</span> {{ $link->created_at }}
                                                </div>
                                            </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection