@extends('Admin/layouts.app')
@section('content')

    <hr>
    <div class="text-center">
        <h3>{{ $processManager->process_name }}</h3>
        <small>{{ $processManager->description }}</small>
    </div>
    <hr>

    @foreach($cycles as $cycle)
        <h5 class="pl-3">Итерация логера: #{{ $cycle->id }}</h5>
        <small class="pl-3">Старт: {{ $cycle->created_at }}</small>
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th scope="col">Имя процесса</th>
                    <th scope="col">Комментарий</th>
                    <th scope="col">Начало</th>
                    <th scope="col">Конец</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cycle->processes as $i => $process)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $process->name }}</td>
                        <td>{!! $process->comment !!}</td>
                        <td>{{ $process->started_at }}</td>
                        <td>{{ $process->ended_at }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    @endforeach

@endsection