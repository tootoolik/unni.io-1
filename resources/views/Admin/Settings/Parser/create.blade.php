{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Создание парсера</h1>
                                <form action="/{{$adminURL}}/settings/paser/create" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group col-lg-4">
                                        <label for="index_elastic">Укажите имя индекса для ElasticSearch</label>
                                        <input type="text" class="form-control" name="index_elastic" id="index_elastic" placeholder="Имя индекса">
                                        <small id="nameIndexHelp" class="form-text text-muted">
                                            Имя можно указывать только на латинице с произвольным именем
                                        </small>
                                        <button type="submit" class="btn btn-primary">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection