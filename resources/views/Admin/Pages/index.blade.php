@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <h1>Страницы</h1>
            <div class="form-group">
                <a href="pages/create" class="btn btn-success">Создать страницу</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Описание</th>
                            <th>Description</th>
                            <th>Keywords</th>
                            <th>Статус публикации</th>
                            <th>Url</th>
                            <th width="100">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{$page->id}}</td>
                                <td>{{$page->title}}</td>
                                <td>{{strip_tags(mb_substr($page->content, 0, 200))}}...</td>
                                <td>{{$page->meta_description}}</td>
                                <td>{{$page->meta_keywords}}</td>
                                <td>{{$page->public}}</td>
                                <td>{{$page->slug}}</td>
                                <td>
                                    <a href="pages/destroy/{{$page->id}}" class="btn btn-xs btn-danger">
                                    Удалить
                                    </a>
                                    <a href="pages/edit/{{$page->id}}" class="btn btn-xs btn-outline-secondary">
                                    Редактировать
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection