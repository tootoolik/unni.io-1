<div id="load">
    @foreach($allTags as $allTag)
        <a href="{{ route('tags.urls', $allTag->id) }}">
            {{$allTag->tag}} ({{$allTag->count}})
        </a>
    @endforeach
    {{ $allTags->links('paginate_one') }}
</div>