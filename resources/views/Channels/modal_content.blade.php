@foreach($users as $user)

    @if($user->avatar)
        <img src="{{ $user->avatar }}" class="img-circle" width="25" alt="avatar">
    @else
        <img src="{{ asset('images/default-avatar.jpg') }}" class="img-circle" width="25" alt="avatar">
    @endif

@endforeach