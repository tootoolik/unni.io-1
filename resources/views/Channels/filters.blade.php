<li class="list-group-item list-domains-item channels-filters-main-block">
    <div class="row channels-lists">

        <div class="col-lg-1 col-md-0"></div>

        <div class="col-lg-9 col-md-12 channels-lists-sort-box">
            <div class="channels-sort-with-other">
                <ul class="list-inline row channels-sort-with-other-list">
                    <li class="col-lg-2 col-md-3 col-sm-3 col-xs-3 channels-sort-other-list-item">
                        <span class="channels-sort-other-list-item-link {{(session('channels_filter_other') === null || session('channels_filter_other') == 'timeline') ? 'channels-sort-other-active' : '' }}"
                              data-type="filter" data-sort="timeline">
                            Новые
                        </span>
                    </li>
                    <li class="col-lg-2 col-md-3 col-sm-3 col-xs-3 channels-sort-other-list-item">
                        <span class="channels-sort-other-list-item-link {{ session('channels_filter_other') == 'popularity' ? 'channels-sort-other-active' : '' }}"
                              data-type="filter" data-sort="popularity">
                            В тренде
                        </span>
                    </li>
                </ul>
            </div>
            <div class="channels-sort-with-date">
                <ul class="list-inline text-center channels-sort-with-date-row">
                    <li class="channels-sort-list-item channels-sort-with-date-all {{ (session('channels_filter_date') === null || session('channels_filter_date') == '-30 years') ? 'channels-sort-by-date-active' : '' }}">
                        <span class="channels-sort-list-item-link channels-sort-with-date-all-link"
                              data-sort="-30 years" data-type="date">Все</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-2 col-md-0"></div>
    </div>
</li>
