<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 16:35
 */

namespace App\Components\ParserLogger\Models;


use Illuminate\Database\Eloquent\Model;

class ParserLoggerCycleProcess extends Model
{
    public $timestamps = false;

    protected $table = 'parser_logger_cycles_processes';

    protected $fillable = [
        'name',
        'comment'
    ];

    public function cycle()
    {
        return $this->belongsTo(ParserLoggerCycle::class, 'cycles_id', 'id');
    }
}