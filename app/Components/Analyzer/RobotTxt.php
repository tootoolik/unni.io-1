<?php

namespace App\Components\Analyzer;

class RobotTxt extends Analyzer
{
    /**
     * Collect all links that the site prohibits indexing.
     *
     * @param string $url
     * @return array
     */
    public function getDisallow(string $url): array
    {
        $disallow = [];

        $contents = $this->getContents($url);

        if($contents)
        {
            if($this->httpStatusCode === 200)
            {
                while (($line = fgets($contents)) != false)
                {
                    if (preg_match('/disallow.*/i', $line))
                    {
                        if($clearUrl = $this->makeClearRule($line))
                        {
                            array_push($disallow, $clearUrl);
                        }
                    }
                }
            }
        }

        return array_unique($disallow);
    }

    /**
     * Return the content resource and generate http status code.
     * Generate a response from the server here because the headers will come after calling fopen().
     *
     * @param string $url
     * @return bool|resource
     */
    private function getContents(string $url)// : ?resource
    {
        $url = $this->makeUrl($url);

        $contents = @fopen($url,'r');

        $this->makeHttpStatusCode($url);

        return $contents;
    }

    /**
     * Current paths to the sitemaps.
     *
     * @param string $url
     * @return array
     */
    public function getPathsToSitemaps(string $url): array
    {
        $path = [];

        $contents = $this->getContents($url);

        if($contents)
        {
            while (($line = fgets($contents)) != false)
            {
                if (preg_match('/^sitemap\s?:/i', $line))
                {
                    $exploded = preg_split('/^sitemap\s?:/i', $line);

                    if(isset($exploded[1]))
                    {
                        $path[] = trim($exploded[1]);
                    }
                }
            }
        }

        return $path;
    }

    /**
     * Compile a link format "protocol://host/robots.txt".
     *
     * @param string $url
     * @return string
     */
    protected function makeUrl(string $url): string
    {
        if(!preg_match('/^(https?):\/\/.*/i', $url))
        {
            $replaced = str_replace('www.','', $url);
            $url = 'http://' . $replaced;
        }

        return $url . '/robots.txt';
    }

    /**
     * Cut spaces and line breaks and look that this is not the homepage.
     *
     * @param string $line
     * @return null|string
     */
    private function makeClearRule(string $line): ?string
    {
        $exploded = explode(':', $line)[1];
        $removedLinebaksAndWhitespace = trim(preg_replace( '/\s*/m', '', $exploded));

        return ($removedLinebaksAndWhitespace !== '/') ? $removedLinebaksAndWhitespace : null;
    }
}