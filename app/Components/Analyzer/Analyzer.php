<?php

namespace App\Components\Analyzer;

abstract class Analyzer
{
    /**
     * Current HTTP Status Code with the file we are analyzing.
     */
    protected $httpStatusCode;

    /**
     * User agent.
     *
     * P.S. Maybe sometimes you need to change the browser version.
     */
    protected $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36';

    /**
     * Analyzer constructor.
     */
    public function __construct()
    {
        ini_set('user_agent', $this->userAgent);
    }

    /**
     * Generate Http status code
     *
     * @param $url
     * @return  void
     */
    protected function makeHttpStatusCode($url) :void
    {
        if((filter_var($url, FILTER_VALIDATE_URL)))
        {
            try
            {
                $headers = array_filter(get_headers($url), function ($header){
                    return strpos($header,'HTTP/') === 0;
                });

                preg_match('/\s\d{3}\s?/', $headers[0],$matches);

                $this->httpStatusCode = (int)trim($matches[0]);
            }
            catch (\Exception $e)
            {
                $this->httpStatusCode = 404;
            }
        }
        else
        {
            $this->httpStatusCode = 404;
        }
    }

    /**
     * The URL does not always have the correct format, we will configure it.
     *
     * @param $url
     * @return mixed
     */
    abstract protected function makeUrl(string $url);
}