<?php

namespace App\Handlers\Events;

use App\Events\StopParser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Stop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StopParser  $event
     * @return void
     */
    public function handle(StopParser $event)
    {
        //
    }
}
