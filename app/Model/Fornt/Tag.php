<?php

namespace App\Model\Fornt;

use App\Front\Domains;
use App\Front\Organizer;
use App\Front\OrganizerCross;
use App\Front\Channels;
use App\Front\ChannelsCross;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    public function pageTags()
    {
        return $this->belongsToMany(Tag::class, 'tags_urls');
    }

    public function urlsTag()
    {
        return $this->belongsToMany(AuditorUrl::class, 'tags_urls', 'tag_id', 'url_id')->groupBy('url_id');
    }

    public function tags($urlId)
    {
        return DB::table('tags_urls')->leftJoin('tags', 'tags.id', '=', 'tags_urls.tag_id')->where('url_id', $urlId)->limit(5)->get();
    }

    public function organizers()
    {
        return $this->belongsToMany(Organizer::class,
            'manual_tags_organizer',
            'tags_id',
            'organizer_id',
            'id',
            'id');
    }

    public function organizersCross()
    {
        return $this->hasMany(OrganizerCross::class,
            'tags_id',
            'id');
    }

    public function channels()
    {
        return $this->belongsToMany(Channels::class,
            'manual_tags_channels',
            'tags_id',
            'channels_id',
            'id',
            'id');
    }

    public function channelsCross()
    {
        return $this->hasMany(ChannelsCross::class,
            'tags_id',
            'id');
    }

    public function domains()
    {
        return $this->belongsToMany(Domains::class, 'tags_domain', 'tag_id', 'domain_id')->orderBy('found_count');
    }
}
