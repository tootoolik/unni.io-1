<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TagsUrls extends Model
{
    protected $table = 'tags_urls';
    protected $fillable = ['url_id', 'tag_id', 'domain_id'];
}
