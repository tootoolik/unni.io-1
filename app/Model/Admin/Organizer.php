<?php

namespace App\Model\Admin;

use App\Model\Tags;
use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $fillable = ['name', 'website', 'fb_group_id', 'city_id', 'country_id'];

    public function manualTags()
    {
        return $this->belongsToMany(Tags::class,'manual_tags_organizer');
    }
}
