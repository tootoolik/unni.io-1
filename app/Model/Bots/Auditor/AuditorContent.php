<?php
namespace App\Model\Bots\Auditor;
use App\Model\Tags;
use Illuminate\Database\Eloquent\Model;
class AuditorContent extends Model
{
    protected $table = 'auditor_content';
    protected $fillable = ['id', 'url_id', 'content', 'domain_id', 'analysis_content', 'analysis_tags', 'analysis_link' ,'created_at', 'updated_at'];

    public function tagsUrls()
    {
        return $this->hasMany(Tags::class, 'url_id', 'id_url');
    }
    public function domain()
    {
        return $this->belongsToMany(AuditorDomains::class, 'auditor_url_children', 'id', 'id_domain', 'url_id');
    }
}
