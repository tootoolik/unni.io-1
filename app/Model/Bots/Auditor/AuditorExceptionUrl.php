<?php

namespace App\Model\Bots\Auditor;

use Illuminate\Database\Eloquent\Model;

class AuditorExceptionUrl extends Model
{
    protected $table = 'auditor_exception_urls';

    protected $fillable = ['url'];
}
