<?php

namespace App\Model\Bots\Auditor;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $fillable = ['website', 'type'];
}
