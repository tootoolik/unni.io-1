<?php

namespace App\Model\Bots\Auditor;

use Illuminate\Database\Eloquent\Model;

class AuditorErrorUrls extends Model
{
    protected $table = 'auditor_error_url';
    protected $fillable =['url_id', 'id_domain', 'error_code', 'message'];
    public function errorsUrl(){
        return $this->hasOne(AuditorUrls::class, 'id', 'url_id');
    }
}
