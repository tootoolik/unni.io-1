<?php

namespace App\Model\Bots\Crons;

use Illuminate\Database\Eloquent\Model;

class MinerCron extends Model
{
    protected $table = "miner_cron";
}
