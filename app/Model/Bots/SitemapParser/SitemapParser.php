<?php

namespace App\Components\SitemapParser;

use App\Facades\Analyzer;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\SitemapUrl;
use Illuminate\Support\Facades\Log;

class SitemapParser
{
    public function run(): void
    {
        $this->writeSitemaps();

//        $this->writeUrls();



//        $allDomains = AuditorDomains::where('parser_content', 1)->where('ping', '!=', 403)->orderBy('count', 'asc')->get();
//
//        foreach ($allDomains as $key => $domain)
//        {
//            $sitemap = Analyzer::sitemap()->getLinks($domain->domain);
//
//            $notFoundsUrls = [];
//
//            foreach ($sitemap as $url)
//            {
//                if(preg_match('/^(.*)\/$/i', $url['loc']))
//                    $url['loc'] = mb_substr($url['loc'], 0, -1);
//
//                $auditorUrl = AuditorUrls::where('url', $url['loc'])->first();
//
//                if($auditorUrl)
//                {
//                    $url['domain_id'] = $domain->id;
//                    $notFoundsUrls[] = $url;
//                }
//
//            }
//
//            echo 'Memory in use (domain): ' . memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <hr>';
//            ob_flush();
//
//        }
    }

    private function writeSitemaps()
    {
        $domains = AuditorDomains::where('parser_content', 1)->where('ping', '!=', 403)->get();

        foreach ($domains as $domain)
        {
            $sitemaps = Analyzer::sitemap()->getSitemaps($domain->domain);
            echo count($sitemaps);
            echo '<br>';
        }

    }

    private function writeUrls()
    {

    }
}