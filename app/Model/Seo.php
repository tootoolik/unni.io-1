<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';

    protected $fillable = ['url' ,'meta_title', 'meta_description', 'title', 'description', 'active'];
}
