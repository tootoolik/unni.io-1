<?php

namespace App\Auditor;

use Illuminate\Database\Eloquent\Model;

class Auditor extends Model
{
    protected $table = 'auditor_domain';
}
