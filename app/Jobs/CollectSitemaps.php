<?php

namespace App\Jobs;

use App\Facades\Analyzer;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Sitemap;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CollectSitemaps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $domain;

    public $retryAfter = 86400; // 1 day

    public $tries = 25;

    public $timeout = 180;

    /**
     * Create a new job instance.
     *
     * @param AuditorDomains $domain
     */
    public function __construct(AuditorDomains $domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemaps = Analyzer::sitemap()->getSitemaps($this->domain->domain);

        $this->domain->sitemaps()->delete();

        foreach ($sitemaps as $url)
        {
            $sitemap = new Sitemap(['url' => $url]);
            $sitemap->domain()->associate($this->domain)->save();

            dispatch(new CheckSitemapUrls($sitemap));
        }
    }
}
