<?php

namespace App\Front;

use App\Model\Fornt\Tag;
use App\Model\Tags;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ChannelsCross extends Model
{
    protected $table = 'manual_tags_channels';

    public function tags()
    {
        return $this->belongsTo(Tag::class, 'tags_id', 'id');
    }

    public function channels()
    {
        return $this->belongsTo(Channels::class, 'channels_id', 'id');
    }
}
