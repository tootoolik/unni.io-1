<?php

namespace App\Front;

use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Bots\Auditor\CountParser;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Fornt\Tag;
use App\Model\Tags;
use Illuminate\Database\Eloquent\Model;

class Domains extends Model
{
    protected $table = 'auditor_domain';

    protected $fillable = ['id', 'domain'];

    /**
     * Получить все записи домена.
     */
    public function domainContent($tags = false)
    {
        $rel = $this->hasManyThrough(JewelerContent::class, AuditorUrls::class, 'id_domain', 'id_url', 'id');

        if($tags) {
            $rel->with('tags');
        }

        return $rel;
    }
//    public function domainTags()
//    {
//        return $this->hasManyThrough(Tags::class, AuditorUrls::class, 'id_domain', 'url_id', 'id')->groupBy('tags.tag')->limit(500);
//    }
//    public function domainTags()
//    {
//        return $this->belongsToMany(Tag::class, 'tags_urls','domain_id', 'tag_id')->limit(100);
//    }
    public function domainTags($limit = 100)
    {
        if($limit === 'all')
            return $this->belongsToMany(Tag::class, 'tags_domain', 'domain_id', 'tag_id')->select('tags_domain.count', 'tags.tag', 'tags.slug', 'tags.id');

        return $this->belongsToMany(Tag::class, 'tags_domain', 'domain_id', 'tag_id')->select('tags_domain.count', 'tags.tag','tags.slug', 'tags.id')->limit($limit);
    }

    public function domainTagsAll()
    {
        return $this->belongsToMany(Tag::class, 'tags_urls', 'domain_id', 'tag_id')->limit(10);
    }

    public function domainTagsHand()
    {
        return $this->hasMany(Tags::class, 'url_id', 'id')->where('type', 'domain');
    }

    public function countLink()
    {
        return $this->hasMany(CountParser::class, 'id_domain', 'id');
    }

    public function count_content()
    {
        return $this->hasMany(CountParser::class, 'id_domain', 'id');
    }

    public function getPreviewChannel()
    {
        $preview = strtok(str_replace('www.', '', $this->domain), '.');
        return '@ ' . $preview;
    }

    public function urls()
    {
        return $this->hasMany(AuditorUrls::class,'id_domain','id');
    }
}
