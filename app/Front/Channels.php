<?php

namespace App\Front;

use App\Model\Fornt\Tag;
use App\Model\Tags;
use App\User;
use App\Admin\Domains;
use Illuminate\Database\Eloquent\Model;


class Channels extends Model
{
    protected $fillable = [
        'name',
        'fb_group_id',
        'website',
        'body',
        'active',
    ];

    public function manualTags()
    {
        return $this->belongsToMany(Tags::class, 'manual_tags_channels');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,
            'manual_tags_channels',
            'channels_id',
            'tags_id',
            'id',
            'id');
    }

    public function manualDomains()
    {
        return $this->belongsToMany(Domains::class, 'manual_domains_channels');
    }

    public function domains()
    {
        return $this->belongsToMany(Domains::class,
            'manual_domains_channels',
            'channels_id',
            'domains_id',
            'id',
            'id');
    }

    public function usersSave()
    {
        return $this->belongsToMany(User::class, 'channels_saves');
    }

    public function usersLike()
    {
        return $this->belongsToMany(User::class, 'channels_likes');
    }

}
