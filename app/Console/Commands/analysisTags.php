<?php

namespace App\Console\Commands;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Bots\JewelerBot\JewelerController;

class analysisTags extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analysis_tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analysis keywords and tags in the head to html, start method analysisTags is controller App\Http\Controllers\Bots\JewelerBot\JewelerController';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        /** START LOGGER **/
        $id = ParserLoggerFacade::startProcess('Создаем и анализируем тэги');
        /** ************ **/

        (new JewelerController())->analysisTags();

        /** STOP LOGGER **/
        ParserLoggerFacade::stopProcess($id);
        /** ************ **/
    }
}
