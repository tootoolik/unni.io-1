<?php

namespace App\Console\Commands;

use App\Front\Channels;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UpdateChannelCounts extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_channel_counts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update channel counts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        parent::handle();

        /**
         */

        $date = Carbon::today();
        $params = [];
        $channels = Channels::where('active', '1')->get();
        if (!empty($channels))
        {
            foreach ($channels as $channel){
                if($channel->manualDomains && count($channel->manualDomains) > 0){
                    foreach($channel->manualDomains as $domain){
                        $params['all_links'] = DB::table('auditor_url_children')
                            ->where('id_domain', $domain->id)
                            ->count();
                        $params['parse_content'] = DB::table('jeweler_content')
                            ->join('auditor_url_children', 'auditor_url_children.id', '=', 'jeweler_content.id_url')
                            ->where('auditor_url_children.id_domain', $domain->id)
                            ->count();
                        $params['count_parse_today'] = DB::table('jeweler_content')
                            ->join('auditor_url_children', 'auditor_url_children.id', '=', 'jeweler_content.id_url')
                            ->where('auditor_url_children.id_domain', $domain->id)
                            ->whereDay('jeweler_content.created_at', $date->format('d'))
                            ->count();
                        DB::table('channels')->where('id', $channel->id)->update($params);
                    }
                }
            }
        }
    }
}
