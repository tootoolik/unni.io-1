<?php

namespace App\Traits\Tag;

use App\Model\Tags;
use Illuminate\Support\Str;

trait TagsTrait
{
    /*
     * Генерируем уникальный slug
     */
    function generateSlug(string $strTag, $id=null): string
    {
        //генерируем slug
        $newSlug= trim(str_slug($strTag, '-'));
        //если надо просто сгенерировать уникальный slug для нового тега -
        // Важно: предпологаем, что уникальность имени ТЕГ уже есть

        if ($id === null){
            $existSlug = Tags::where([['slug','=',$newSlug]])->first();
            if($existSlug)
            {
                $slug = Str::limit($newSlug,64).'-'.str_random(5);
            }else{
                $slug = $newSlug;
            }
        }
        //если надо просто сгенерировать уникальный slug
        else{
            $existSlug = Tags::where([['slug','=',$newSlug],['id','!=', $id]])->first();
            if($existSlug)
            {
                $slug = Str::limit($newSlug,64).'-'.$id;
            }else{
                $slug = $newSlug;
            }
        }
        return $slug;
    }
}
