<?php

use App\Model\Seo;

if (! function_exists('get_seo_value'))
{
    function get_seo_value($field = null)
    {
        $requestPath = request()->path();

        if($requestPath !== '/')
            $requestPath = '/' . $requestPath;

        $seoItem = Seo::where('url', $requestPath)->first();

        return ($seoItem && $seoItem->active) ? $seoItem->$field : null;
    }
}