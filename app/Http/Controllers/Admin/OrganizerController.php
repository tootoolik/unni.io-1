<?php

namespace App\Http\Controllers\Admin;

use App\Front\Organizer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateUpdateOrganizerRequest;
use App\Model\Admin\City;
use App\Model\Admin\Country;
use Illuminate\Http\Request;

class OrganizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('Admin.Organizers.index', ['organizers' => Organizer::orderBy('created_at', 'desc')->paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = Country::get();
        return view('Admin.Organizers.show', ['country' => $country]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUpdateOrganizerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUpdateOrganizerRequest $request)
    {
        $organizer = Organizer::create([
            'name' => $request->name,
            'website' => $request->website,
            'fb_group_id' => $request->fb_group_id,
            'city_id' => $request->city,
            'country_id' => $request->country,
            'body_m' => $request->body_m,
        ]);

        $this->storeManualTags($organizer, $request);

        return redirect()->route('admin.organizers.index')->with('success', 'Новый организатор успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param Organizer $organizer
     * @return \Illuminate\Http\Response
     */
    public function show(Organizer $organizer)
    {
        $country = Country::get();
        $countryId = $this->getCountryId($organizer);

        $tags = $organizer->manualTags;

        return view('Admin.Organizers.show', ['organizer' => $organizer, 'country' => $country, 'countryId' => $countryId, 'tags' => $tags]);
    }

    private function getCountryId($organizer)
    {
        if($organizer->county_id > 0)
            return $organizer->county_id;

        $city = City::find($organizer->city_id);

        return $city ? $city->first()->country->id : null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Organizer $organizer
     * @return void
     */
    public function edit(Organizer $organizer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateUpdateOrganizerRequest $request
     * @param Organizer $organizer
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUpdateOrganizerRequest $request, Organizer $organizer)
    {
        Organizer::where('id', $organizer->id)->update([
            'name' => $request->name,
            'website' => $request->website,
            'fb_group_id' => $request->fb_group_id,
            'city_id' => $request->city,
            'country_id' => $request->country,
            'body_m' => $request->body_m,
            'created_at' => $organizer->created_at
        ]);

        $this->storeManualTags($organizer, $request);

        return back();
    }

    public function storeManualTags($organizer, $request)
    {
        $organizer->manualTags()->detach();

        if ($request->has('tags'))
        {
            foreach ($request->tags as $tagId)
            {
                $organizer->manualTags()->attach([
                    'tags_id' => $tagId
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Organizer $organizer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organizer $organizer)
    {
        $organizer->manualTags()->detach();
        Organizer::destroy($organizer->id);
        return back();
    }

    public function loading_city(Request $request)
    {
        if ($request->country_id)
        {
            $cities = City::where('country_id', $request->country_id)->orderBy('name_ru', 'asc')->get();
            if ($cities){
                return view('Admin.Organizers.Forms.city', ['cities' => $cities, 'city_id' => $request->city_id]);
            }
        }
    }
}
