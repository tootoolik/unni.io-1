<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Domains;
use App\Model\Admin\FollowLink;
use App\Model\Admin\Tag;
use App\Traits\ArrayUnion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    use ArrayUnion;
    public function index()
    {
        return view('Admin.Statistics.index');
    }
    public function tags()
    {
        $tags = Tag::select(DB::raw('DATE(updated_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('updated_at', 'desc')
            ->limit(5)
            ->get();
        $tagsNew = Tag::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();
        $popularTags = Tag::where('type', 'parser')->orderBy('count', 'desc')->paginate(5);
        return view('Admin.Statistics.tags', [
            'tags' => $tags,
            'tagsNew' => $tagsNew,
            'popularTags' => $popularTags
        ]);
    }
    public function chartNewTags()
    {

    }
    public function domains()
    {
        return view('Admin.Statistics.domains');
    }
    public function domainsParams()
    {
        return Domains::all();
    }
    public function  clickLinks()
    {
        $links = FollowLink::paginate(25);
        return view('Admin.Statistics.clickLinks', ['links' => $links]);
    }
}
