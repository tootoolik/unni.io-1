<?php

namespace App\Http\Controllers\Bots\ElasticIndex;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Controller;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Tags;
use Elasticsearch\ClientBuilder;

class ElasticMetaController extends Controller
{
    const LIMIT_ELASTIC_INDEXES = 200;

    private $createIndexesLoggerId = '';
    private $countNewIndexes = 0;

    /**
     * Метот временно остановлен до выяснения обстоятельств
     * PS писал прошлый разраб
     */
    public function elasticIndexisTags(){
        $tags = Tags::where('elastic_index', 0)->limit(50)->get();
        foreach ($tags as $tag) {
            $params = [
                'index' => 'jeweler_tags',
                'type' => 'my_type',
                'id' => $tag->id,
                'body' => [
                    'tag' => $tag->tag,
                    'url_id' => $tag->url_id,
                    'created_at' => $tag->created_at
                ],
            ];
            $client = ClientBuilder::create()->build();
            $client->index($params);
            Tags::where('url_id', $tag->url_id)->update(['elastic_index' => 1]);
        }
    }

    /**
     * Выбираем разобранный контент мета данных и отправлям в индекс для ElasticSearch,
     * после ставим метку elastic_index в таблицу jeweler_content,
     * что информирует об индексации документа.
     *
     * @return void
     */
    public function elasticIndexisContent(): void
    {
        $this->createIndexesLoggerId = ParserLoggerFacade::startProcess('Создание индексов');

        $contents = JewelerContent::with('linksContent')
                                  ->where('elastic_index', 0)
                                  ->limit(self::LIMIT_ELASTIC_INDEXES)
                                  ->get();

        foreach ($contents as $content)
        {
            if (strlen($content) > 0)
            {
                if (isset($content->linksContent->url))
                {
                    ++$this->countNewIndexes;
                    $params = [
                        'index' => 'jeweler_content',
                        'type' => 'my_type',
                        'id' => $content->id,
                        'body' => [
                            'description' => $content->description,
                            'image' => $content->image,
                            'created_page' => $content->created_page,
                            'title' => $content->title,
                            'id_url' => $content->id_url,
                            'url' => $content->linksContent->url,
                            'id_domain' => $content->linksContent->id_domain,
                            'created_at' => array_only((array)$content->created_at,['date','timezone_type','timezone'])
                        ],
                    ];
                    $client = ClientBuilder::create()->build();
                    $client->index($params);
                }
            }

            JewelerContent::where('id_url', $content->id_url)->update(['elastic_index' => 1]);
        }

        $this->elasticIndexisContentLogging($contents);
    }


    /**
     * Пишем в кастомный лог результат работы elasticIndexisContent()
     *
     * @param $contents
     *
     * @return void
     */
    private function elasticIndexisContentLogging($contents) :void
    {
        if($contents->count() > 0)
        {
            $loggerData = '<strong>Кол-во выбранного контента: </strong> ' . $contents->count() . '<br>' .
                '<strong>Кол-во новых индексов: </strong> ' . $this->countNewIndexes . '<br>';

            ParserLoggerFacade::stopProcess($this->createIndexesLoggerId, $loggerData);
        }
        else
        {
            ParserLoggerFacade::offAndRemove();
        }
    }
}
