<?php

namespace App\Http\Controllers;

use App\Front\Domains;
use App\Model\Bots\Miner\MinerContent;
use App\Model\Dictionary;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomePageController extends Controller
{
    public function liveSearch(Request $request){
//        $posts = DB::table('jeweler_content')
//            ->where('title', 'like', '%'.$request->keywords.'%')
//            ->get();
//        $keywords = $request->keywords;
//
//        $posts = DB::table('miner_content')->
//            where(function ($q) use ($keywords) {
//            $q->where('title', 'like', '%'.str_replace(' ', '', $keywords).'%')
//                ->orWhere('description', 'like', "%{$keywords}%");
//        })->get();
        if (!empty($request->keywords)){
            $client = ClientBuilder::create()->build();
            $params['index'] = 'jeweler_content';
            $params['type'] = 'my_type';
            $params['from'] = $request->page-1;
            $params['size'] = 10;
                $params['body'] = ['query' => [
                    'match' => [
                        'description' => $request->keywords
                    ]
                ]];
            $accounts = $client->search($params);
            $posts = $accounts['hits']['hits'];
        }

        return response()->json($posts);
    }

    public function tips(Request $request)
    {
        $tips = Dictionary::where('title', 'like', $request->phrase.'%')->limit(10)->get();
        $chanels = Domains::where('domain', 'like', $request->phrase.'%')->limit(10)->get();
//        foreach ($tips as $tip) {
//            echo '<li>'.$tip->title.'</li>';
//        }
        return $tips->toArray();
    }
}
