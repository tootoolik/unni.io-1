<?php

namespace App\Http\Controllers;

use App\Front\Domains;
use App\Model\Bots\Auditor\AuditorContent;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Fornt\AuditorUrl;
use App\Model\Tags;
use App\Model\TagsDomain;
use App\Model\TagsUrls;
use App\Traits\BustGetUrl;
use App\Traits\HtmlDomParsing;
use Curl\MultiCurl;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    use HtmlDomParsing, BustGetUrl;

    protected $socialTags = ['article:tag', 'article:section'];
    protected $socialTagsSection = ['article:section'];
    protected $socialTagArt = ['og:image'];
    protected $publishedTime = ['article:published_time'];
    protected $datePublished = ['datePublished'];
    protected $charset = ['content-type'];
    private $alphabetRus = '"АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя"';
    protected $dom = null;

    private $requiredFileds = [
        'loc',
        'lastmod',
        'changefreq',
        'priority'
    ];

    const ANALYSIS_LINK_LIMIT = 700;
    const ANALYSIS_CONTENT_LIMIT = 700;
    const ANALYSIS_TAG_LIMIT = 600;
    const LIMIT_URLS_TO_TMP_TABLE = 200;
    const LIMIT_URLS_FROM_TMP_TABLE = 200;
    public $urls = [];


    /** DATA FOR LOGGER **/
    private $checkDateFromOG = 0;
    private $checkDateFromSchema = 0;
    private $checkDateFromDB = 0;

    protected $isSitemap = false;

    protected $analysisContentCount = 0;
    protected $allLinksCount = 0;
    protected $allLinksApprovedCount = 0;
    protected $allLinksNewsCount = 0;

    protected $updateOrCreateSec = 0;

    const LIMIT_DOMAINS = 20;
    const LIMIT_SYMBOL_OF_TEXT = 1000000;

    private $multiCurl;

    // Logger data parse()
    private $internalPageCount = 0;
    private $homePageCount = 0;
    private $analysisLinksData = '<span>---- MultiCurl не был вызван ----</span><br>';
    private $countAllLinks = 0;

    // Logger data for urlAnalysisLinks()
    private $analysisLinksLoggerId = '';

    // Logger data for tableTmp()
    private $tableTmpLoggerId = '';
    private $countListDomains = 0;
    private $countListDomainsWriteToTmpTable = 0;
    private $countCreatedTables = 0;
    private $countDropTables = 0;
    private $countUpdateTables = 0;

    // Logger data for startParseUrls()
    private $startParseUrlsLoggerId = '';
    private $parseCronLoggerData = false;


    /*
     * auditor_count - счетчики
     * auditor_domain (вторую очередь)
     * auditor_error_url - HTTP коды ответов ссылок
     * auditor_url_anhors - она не нигде не используется
     * auditor_url_children - ссылки
     * jobs - системные данные
     * jeweler_content - контент
     * parser_logger_cycles - данные логера
     * parser_logger_cycles_processes - данные логера
     * tags - тэги
     * tags_section
     * tags_urls
     * manual_tags_organizer
     *
     * sitemaps - сайтмапы домена
     * sitemap_urls - урлы сайтмапа домена
     *
     * DB unni_tmp_url - ссылки для парсинга
     * ElasticSearch - jeweler_content
     *
     *

    $dd = AuditorDomains::where('type', '!=', 'admin')->get();
    foreach ($dd as $d)
        $d->delete();

     * */



    public function 🔧🔨⚒()
    {
        $this->analysisTags();
    }

    public function analysisTags() :void
    {
        $contents = AuditorContent::where('analysis_tags', 0)->limit(100)->get();

        foreach ($contents as $content)
        {
            $keywords = $this->keywords($content->content);

            if ($keywords)
            {
                $tags = [];
                foreach ($keywords as $keyword)
                {
                    $keyword = $this->deleteSpecialСharacters($keyword);

                    if ($this->mbStrWordCount($keyword) < 7 && $keyword !== '' && $keyword !== ' ')
                    {
                        $tags[] = $this->prepareTagsForWriting($keyword, $content);
                    }
                }

                dump($tags);

            }
        }
    }

    private function prepareTagsForWriting($keyword, $content)
    {

        $kw = trim($keyword);
        $tag = Tags::where('tag',$kw)->first();
        if(!$tag){
            Tags::Create([
                'tag' => $kw,
                'slug' => $this->generateSlug($kw),
                'type' => 'parser',
            ]);
        }

        $tag->increment('count');

        TagsDomain::firstOrCreate(
            ['domain_id' => $content->domain_id, 'tag_id' => $tag->id],
            ['type' => 'parser']
        )->increment('count');

        return [
            'url_id' => $content->url_id,
            'domain_id' => $content->domain_id,
            'tag_id' => $tag->id
        ];
    }

    private function mbStrWordCount($str)
    {
        $alphabets = 'АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя';
        return str_word_count($str, 0, $alphabets);
    }

    protected function parse($domains, $home = null)
    {
        $this->initializeMultiCurl();

        $this->multiCurlParseSuccess();
        $this->multiCurlParseError();

        $this->runMultiCurlForDomains($domains, $home);

        if(isset($this->multiCurl)) $this->multiCurl->start();

//        return $this->parseLogging($domains);
    }

    private function runMultiCurlForDomains($domains, $home)
    {
        foreach ($domains as $domain)
        {
            if ($home === null)
            {
                $mk = $this->multiCurl->addGet($domain->url);
                $mk->url_id = $domain->url_id;
                $mk->urlsss = $domain->urlsss;
                $mk->id_domain = $domain->id_domain;
            }
            else
            {
                $domain->url = 'http://' . $domain->domain;
                $domain->url_id = 'home';
                $mk = $this->multiCurl->addGet($domain->url);

                $mk->url_id = $domain->url_id;
                $mk->urlsss = $domain->urlsss;
                $mk->id_domain = $domain->id;

            }
        }

    }

    private function initializeMultiCurl() :void
    {
        $mk = new MultiCurl();
        $mk->setReferer(true);
        $mk->setTimeout(30);
        $mk->setOpt(CURLINFO_SSL_ENGINES, 1);
        $mk->setConnectTimeout(30);
        $mk->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $mk->setOpt(CURLOPT_ENCODING, '');
        $mk->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $mk->setHeader('Content-Type', 'text/html');
        $mk->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');

        $this->multiCurl = $mk;
    }

    private function multiCurlParseSuccess()
    {
        $this->multiCurl->success(function ($instance)
        {
            if ($instance->url_id != 'home')
            {
                ++$this->internalPageCount;
                $instance->httpPort = $instance->getInfo()['primary_port'];
                $this->analysisLinksData = $this->urlAnalysisLinks($instance);
                DB::connection('mysqlTmpUrl')->table($instance->urlsss)->where('url_id', $instance->url_id)->delete();
            }
            else
            {
//                ++$this->homePageCount;
//                $instance->httpPort = $instance->getInfo()['primary_port'];
//                $this->analysisLinksData = $this->urlAnalysisLinks($instance);
            }

        });

    }

    private function multiCurlParseError()
    {
        $this->multiCurl->error(function ($instance)
        {
            echo 'error';
            dd($instance);
        });

    }

    private function showIteration($i, $cyclesCount)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        echo '<script id="scri">document.getElementById("remove").remove();document.getElementById("scri").remove()</script>';
        echo '<div id="remove">';
        echo ++$i . ' из ' . $cyclesCount;
        echo '</div>';
        ob_flush();
    }

    public function test()
    {
        $url = 'https://qna.habr.com/q/61<988';
        if (preg_match('/[*"*;*@*`*~*{*}*,*\\\*^*<*>*\'*|*]/', $url)) {

            dd( 'с ошибкой',$url );
        }
        dd( 'все ок',$url );

    }
}
